﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace KeyBoardCharacters
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter A Keyboard Character");
            var input = Console.ReadLine();

           var keyboardChar = new Dictionary<char, string> 
            {
                { ')', "1" },
                { '(', "2" },
                { '*', "3"},
                { '&', "4" },
                { '^', "5" },
                { '%', "6" },
                { '$',"7"  },
                { '#', "8" },
                { '@', "9" },
                { '!',"0" }
            };

            foreach(var b in input) // ) ( * ^ $ )
            {
                if (keyboardChar.ContainsKey(b))
                {
                    Console.Write(keyboardChar[b]);
                }
            }




            var querySyntax = from x in input
                              where keyboardChar.ContainsKey(x)
                              select keyboardChar[x];

            Console.WriteLine("Where in querySyntax------");

            foreach (var characters in querySyntax)
            {
                Console.Write(characters);
                
            }



        }
    }
}
