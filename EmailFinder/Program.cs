﻿using System;
using System.Text.RegularExpressions;
namespace EmailFinder
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Console.WriteLine("enter a text with emails included");

            string texts = Console.ReadLine();



            var checkEmail = ValidateEmail(texts);
            foreach (var i in checkEmail)
            {
                Console.WriteLine(i);
            }
        }


        static MatchCollection ValidateEmail(string text)
        {
            // define regex with email pattern
            Regex regex = new Regex(@"[a-zA-Z0-9._%+-]+@[a-zA-Z]+(\.[a-zA-Z0-9]+)+", RegexOptions.IgnoreCase);


            MatchCollection emailMatch = regex.Matches(text);
            return emailMatch;


        }
    }
}
