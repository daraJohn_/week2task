﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;

namespace LoginSystem
{
    class Application
    {

        

        public static void Run()

        {
            var cbn = new CBN();
          

            Regex regex = new Regex(@"[a-zA-Z0-9._%+-]+@[a-zA-Z]+(\.[a-zA-Z0-9]+)+", RegexOptions.IgnoreCase);
            Regex account = new Regex(@"^\d{10}$");
            Regex NameValidation = new Regex(@"^[a-zA-Z]+$");


            Console.ForegroundColor = ConsoleColor.Green;

            Console.WriteLine(" Welcome To The Central Bank BVN CHECK System\n");

            Console.WriteLine("Click 1 To Check Your BVN and 2 to Enroll");

            string usage = Console.ReadLine();

            while (usage != "1" && usage != "2")
            {
                Console.ForegroundColor = ConsoleColor.Red;
                usage = RePrompt("detail");
                Console.ResetColor();
            }


            if (usage == "1")
            {
                Console.WriteLine("Hello, To Check Your BVN, Please Enter Your Name");
                string name = Console.ReadLine().Trim().ToLower();

                while (NameValidation.IsMatch(name) == false)
                {
                    name = RePrompt("name");
                }

                Console.WriteLine("enter your account number");
                string accountNumber = Console.ReadLine();
                while (account.IsMatch(accountNumber) == false)
                {
                    accountNumber = RePrompt("accountNumber");
                }

                cbn.FindUser(accountNumber,name);
                

            }else if (usage == "2")
            {
                Console.WriteLine("Hello, To Begin your Enrollment Process, Please enter your Name");
                string name = Console.ReadLine().Trim();

                while (NameValidation.IsMatch(name) == false)
                {
                    name = RePrompt("name");
                }

                Console.WriteLine("\nSet your 4-digit PIN");
                string pin = Console.ReadLine().Trim();

                while (string.IsNullOrWhiteSpace(pin) || pin.Length < 4 || pin.Length > 4)
                {
                    pin = PromptPin();
                }

                Console.WriteLine("enter your valid email address");
                string email = Console.ReadLine();

                while (regex.IsMatch(email) == false)
                {
                    email = RePrompt("email");
                }

                Console.WriteLine("enter your account number");

                string accountNumber = Console.ReadLine();
                while (account.IsMatch(accountNumber) == false)
                {
                    email = RePrompt("email");
                }

                CBN.EnrollUser(accountNumber, name);
            }

            static string RePrompt(string fieldName)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Please, enter a valid {fieldName}");
                Console.ForegroundColor = ConsoleColor.White;
                return Console.ReadLine().Trim();
            }

            static string PromptPin()
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\nPIN must be 4-digits!");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("\nSet your 4-digit PIN");
                return Console.ReadLine().Trim();
            }


            static string GenerateNumber()
            {
                Random random = new Random();
                string r = "";
                int i;
                for (i = 1; i < 12; i++)
                {
                    r += random.Next(0, 9).ToString();
                }
                return r;
            }

           
                //WHERE  
                



            




        }



     
                //static bool ValidateEmail(string text)
                //{
                //    // define regex with email pattern
                //    Regex regex = new Regex(@"[a-zA-Z0-9._%+-]+@[a-zA-Z]+(\.[a-zA-Z0-9]+)+", RegexOptions.IgnoreCase);


                //    if (regex.IsMatch(text)) {
                //        return true};



                //}
            
    }
}
